# User-post-dashbord

This is a React single-page application that allows users to view a table of users, select a user to view their posts, and create new posts using PrimeReact components.

## Features

- **UsersTable**: Displays a table of users fetched from an API, allowing users to be filtered by name and email.
- **UserPosts**: Displays posts by a selected user on the right side of the screen.
- **NewPost**: Enables users to create a new post for the selected user with validation.

## Installation

1. Clone the repository:   
   git clone https://gitlab.com/moriya-vaizer/user-post-dashbord.git
  
2. Navigate to the project directory:
 
   cd user-post-dashboard
  
3. Install dependencies:

   npm install
  
## Usage

1. Start the development server:

   npm start
  
2. Open your browser and navigate to `http://localhost:3000` to view the application.

## Components

- **UsersTable**: Displays a table of users with columns for name, email, and company name. Users can be filtered by name or email.
  
- **UserPosts**: Shows a list of posts by the selected user on the right side of the screen. Posts are fetched from an API.

- **NewPostDialog**: A dialog component that allows users to create a new post for the selected user. It includes form fields for the post title and body with validation.

## API Integration

- Data for users and posts is fetched from the [JSONPlaceholder](https://jsonplaceholder.typicode.com/) API.

## Dependencies

- React
- PrimeReact
- Axios
