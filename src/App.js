import './App.css';
import "primereact/resources/themes/bootstrap4-light-blue/theme.css"
import 'primeicons/primeicons.css';        
import UsersTable from './components/UsersTable/UsersTable';

// Render UsersTable component
function App() {

  return (
    <UsersTable />
  );

}

export default App;
