import React, { useState, useEffect, useRef } from 'react';
import { ListBox } from 'primereact/listbox';
import { getPostsByUserId } from '../../services/api';
import { Toast } from 'primereact/toast';
import { Button } from 'primereact/button';
import { ProgressSpinner } from 'primereact/progressspinner';
import NewPostDialog from '../NewPostDialog/NewPostDialog';
import './UserPosts.css';


// UserPosts component to display posts for a user

const UserPosts = ({ userId, userName }) => {

  // State hooks for posts, dialog visibility and loading state
  const [posts, setPosts] = useState([]);
  const [newPostDialogVisible, setNewPostDialogVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  // Toast ref for error notifications
  const toast = useRef(null);

  // Handler to hide new post dialog
  const handleNewPostDialogHide = () => {
    setNewPostDialogVisible(false);
  };

  // Fetch posts on mount using userId
  async function fetchPosts() {
    setIsLoading(true);

    // Get cached posts if available
    const cachedPosts = localStorage.getItem('userPosts' + userId);
    if (cachedPosts) {
      setPosts(JSON.parse(cachedPosts));
      setIsLoading(false);
    } else {
      try {
        // Fetch posts via API
        const response = await getPostsByUserId(userId);
        setPosts(response.data);
        cachePosts(response.data);
      } catch (error) {
        showError(error);
      } finally {
        setIsLoading(false);
      }
    }
  }
// eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    fetchPosts();
  }, [userId]);


  // Cache posts in localStorage
  const cachePosts = (posts) => {
    localStorage.setItem('userPosts' + userId, JSON.stringify(posts));
  }

  // Handler when a new post is created
  const handleNewPostCreated = (newPost) => {

    // Get cached posts if available
    let updatedPosts = localStorage.getItem('userPosts' + userId);
    if (updatedPosts) {
      updatedPosts = JSON.parse(updatedPosts);
    }
    else {
      // Fetch latest posts
      getPostsByUserId(userId)
        .then(response => {
          updatedPosts = response.data;
        })
        .catch(error => {
          showError(error);
        });
    }

    // Add new post
    let post = JSON.parse(newPost);
    updatedPosts.push({
      userId: post.userId,
      title: post.title,
      body: post.body
    });

    // Update state and cache
    setPosts(updatedPosts);
    cachePosts(updatedPosts);
  };

  // Show error toast
  const showError = (error) => {
    toast.current.show({
      severity: 'error',
      summary: 'Error',
      detail: 'Error loading data'
    });
  }

  return (
    <>
      {isLoading && <ProgressSpinner />}

      <div className='user-posts'>
        <h2>{userName} Posts</h2>

        <ListBox
          options={posts}
          optionLabel="title"
          className="w-full md:w-14rem"
          listStyle={{ height: '25rem', width: '30rem' }}
          itemTemplate={(post) => (
            <div className="post-item">
              <h3>{post.title}</h3>
              <p>{post.body}</p>
            </div>
          )}
        />


        <NewPostDialog
          visible={newPostDialogVisible}
          onHide={handleNewPostDialogHide}
          onPostCreated={handleNewPostCreated}
          userId={userId}
        />

        <Button
        id="create-post-button"
          onClick={() => setNewPostDialogVisible(true)}
        >
          Create Post
        </Button>

        <Toast ref={toast} position='top-center' />
      </div>
    </>
  );
};


export default UserPosts;
