import React, { useState, useRef } from 'react';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Editor } from 'primereact/editor';
import { createPost } from '../../services/api';
import { Toast } from 'primereact/toast';
import './NewPostDialog.css';



// Dialog component to create a new post
const NewPostDialog = ({ visible, onHide, onPostCreated, userId }) => {

  // State for form values
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');

  // State for loading indicator
  const [isLoading, setIsLoading] = useState(false);

  // Toast for error messages
  const toast = useRef(null);

  // Reset form
  const resetForm = () => {
    setTitle('');
    setBody('');
  }

  // Handle form submission
  const handleSubmit = async () => {

    // Validate form  
    if (!title || !body) {
      showMessage('Title and body are required', 'error');
      return;
    }

    // Show loading indicator  
    setIsLoading(true);

    try {
      // Create new post  
      const response = await createPost({ title, body, userId });

      // Pass new post back to parent
      onPostCreated(response.data.body);

      // Hide dialog
      onHide();

      // Show success toast
      showMessage('Post created successfully', 'success');

    } catch (error) {
      console.error('Error creating post', error);

      // Show error toast
      showMessage('Error creating post');
    } finally {
      // Hide loading indicator
      setIsLoading(false);


      resetForm();
    };

  }
 

  // Handle cancel button click
  const handleCancel = () => {
    onHide();
    resetForm();  
  }

  // Show error toast
  const showMessage = (message, type) => {
    toast.current.show({
      severity: type,
      summary: type,
      detail: message,
      life: 3000
    });
  };

  return (
    <>
      <Dialog
        visible={visible}
        onHide={onHide}
        header="Create New Post"
      >

        {/* Form */}
        <div className="p-fluid">

          <div className="p-field">
            <label htmlFor="title" >Title</label>
            <InputText
              id="title"
              value={title}
              onChange={e => setTitle(e.target.value)}
            />
          </div>

          <label htmlFor="body" className='dialog-elements'>Body</label>
          <div className="card flex justify-content-center">
            <Editor
              value={body}
              onTextChange={(e) => setBody(e.textValue)}
              style={{ height: '320px', border: '1px solid #d9d9d9', fontSize: '1.3rem' }}
            />
          </div>
        </div>

        {/* Buttons */}
        <div className="p-d-flex p-jc-end">
          <Button
            label="Cancel"
            className="p-button-text"
            onClick={handleCancel}
            raised

          />
          <Button
            label="Create"
            className="p-button"
            onClick={handleSubmit}
            loading={isLoading}
            raised
            icon="pi pi-check"
          />
        </div>

      </Dialog>

      {/* Error Toast */}
      <Toast ref={toast} position="top-center" />
    </>
  );
};


export default NewPostDialog;
