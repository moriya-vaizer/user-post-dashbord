import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Toast } from 'primereact/toast';
import { ProgressSpinner } from 'primereact/progressspinner';
import { getUsers } from '../../services/api';
import UserPosts from '../UserPosts/UserPosts'
import './UsersTable.css';

// UsersTable component to display users table

const UsersTable = ({ onSelectUser }) => {

  // State to store users list
  const [users, setUsers] = useState([]);

  // State to store selected user
  const [selectedUser, setSelectedUser] = useState(null);

  // State to control row click
  const [rowClick, setRowClick] = useState(true);

  // State to show loading indicator
  const [isLoading, setIsLoading] = useState(false);

  // Ref for toast
  const toast = useRef(null);

  // Fetch users on initial render
  useEffect(() => {
    setRowClick(true);
    setIsLoading(true);
    const fetchUsers = async () => {
      try {
        const response = await getUsers();
        setUsers(response.data);
      } catch (error) {
        showError();
        console.error('Error fetching users:', error);
      } finally {
        setIsLoading(false); // Stop loading
      }
    };
    fetchUsers();
  }, []);

  // Handle user selection
  const handleUserSelect = async (e) => {
    setSelectedUser(e.value);
    if (onSelectUser) {
      await onSelectUser(e.value);
    }
  };

  // Show error toast
  const showError = () => {
    toast.current.show({
      severity: 'error',
      summary: 'Error',
      detail: 'Error loading data'
    });
  }

    return (
    <>
      {/* Show loading spinner */}
      {isLoading &&
        <ProgressSpinner
          style={{ width: '50px', height: '50px' }}
          strokeWidth="8"
        />
      }

      {/* Display title */}
      <h1 className="title">Users</h1>

      {/* Display users table */}
      
      <div className='display-posts'>
        <DataTable
          className="p-datatable-users"
          value={users}
          filterDisplay="row"
          globalFilterFields={['name', 'email']}
          onSelectionChange={handleUserSelect}
          selectionMode={rowClick ? null : 'checkbox'}
          selection={selectedUser}
          dataKey="id"
        >
          

          <Column
            filter
            filterPlaceholder="Search by name"
            field="name"
            header="Name"
          ></Column>

          <Column
            field="email"
            filter
            filterPlaceholder="Search by email"
            header="Email"
          ></Column>

          <Column
            field="company.name"
            header="Company Name">
          </Column><Column
          header="posts"
          className='p-column-selection'
            selectionMode="single"
            headerStyle={{ width: '2rem'}}
          ></Column>
        </DataTable>

        {/* Display posts for selected user */}
        {selectedUser &&
          <UserPosts
            userId={selectedUser.id}
            userName={selectedUser.name}
          ></UserPosts>
        }
      </div>

      {/* Toast */}
      <Toast
        ref={toast}
        position='top-center'
      />

    </>
  );
};

export default UsersTable
