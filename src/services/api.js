import axios from 'axios';

let postsAmount;// Keep track of total number of posts

const apiClient = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com',
});

export const getUsers = () => {
  return apiClient.get('/users');
};

export const getPostsByUserId = (userId) => {
  return apiClient.get(`/posts?userId=${userId}`);
};
export const getPosts = () => {
  return apiClient.get('/posts');
};
export const createPost = async (postData) => {

  // Get current number of posts from local storage
  postsAmount = localStorage.getItem('postsAmount');

  // If postsAmount exists, increment by 1
  if (postsAmount) {
    postsAmount = parseInt(postsAmount) + 1;
    localStorage.setItem('postsAmount', postsAmount);
  
  // Otherwise get total posts from API
  } else {
    const response = await getPosts();
    postsAmount = response.data.length + 1;
    localStorage.setItem('postsAmount', postsAmount);
  }

  // Make API call to create new post
  const result = await apiClient.post('/posts', {
    method: 'POST', 
    body: JSON.stringify({
      userId: postData.userId,
      id: postsAmount,  
      title: postData.title,
      body: postData.body
    }),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  });

  return result;

};
